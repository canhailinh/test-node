FROM node:16-alpine

WORKDIR /app

RUN npm install -g pm2

COPY ./tolnft-front/ .
RUN npm install \
    && yarn run build
EXPOSE 3000

CMD ["yarn", "next", "start"]
